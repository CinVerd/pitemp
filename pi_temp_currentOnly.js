var http = require('http');
var util = require('util');
var fs = require('fs');
var url = require('url');

//Function reads the current temperature of the RaspberryPi (which is stored as a readable file), and returns it in JSON format.
function getCurrentTemperature(callback){
	var temp = fs.readFileSync('/sys/class/thermal/thermal_zone0/temp');
		
	var tempActual = temp/1000;
	console.log("Temperature read as " + tempActual);
	
	//The structure used to store details in JSON format.
	var data = { tempReading: []};
	
	data.tempReading.push({
				"temperature" : tempActual,
				});
	//Call the function that was passed in as an argument to this function
	callback(data);
	
}

//This function handles all requests (req) sent to this server (referenced on line 56, when the server is started), and the responses sent back (res)
function handler(req, res){
  //This server won't support POST, because there's nothing we'll ever want to submit to the server, we just want it to send back data when a GET is sent
  if(req.method == "GET"){ 

	//We've gotten a get request, now lets determine what path was sent in the URL.
	//The path is the string appended to the GET request after the port, for example localhost:1138/MyPathHere
	var q = url.parse(req.url, true);
	var pathname = q.pathname;

	//At the moment there is only one implemented path, sending anything else just won't be handled.
	if (pathname == "/currenttemperature"){
		
		//Call the function that reads the temperature, and pass in a function as a callback that returns that data using the server "response" variable
		getCurrentTemperature(function (currentTemperature){
			//If this wasn't implemented as a callback function, the return statement in getCurrentTemperature might return before the data is actually read
			res.setHeader('Content-Type', 'application/json');
			
			//It's possibly unneeded to use JSON.stringify here, as the currentTemperature variable should already be in JSON format
			res.end(JSON.stringify(currentTemperature));
		});

	}else{
		console.log("Unknown request: " + pathname);
	}
	
  }else {
  //if any method other than GET is used, we just return the HTTP 200 status code, OK. This indicates the server got the request, but it isn't going to do anything.
    res.writeHead(200);
    res.end();
  };
};

// Create a server that invokes the `handler` function upon receiving a request
// And have that server start listening for HTTP requests
// The callback function is executed at some time in the future, when the server
// is done with its long-running task (setting up the network and port etc)
http.createServer(handler).listen(1138, function(err){
  if(err){
    console.log('Error starting http server');
  } else {
    console.log('Server listening on port 1138');
  };
});